<?php

/**
 * @file
 * merci ui default views 
 */


/**
 * Implementation of hook_views_default_views().
 */
function merci_signup_views_default_views() {

  $view = new view;
  $view->name = 'merci_signup_unpublished_volunteer_requests';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = TRUE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
	'title' => array(
	  'label' => 'Title',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 1,
	  'exclude' => 0,
	  'id' => 'title',
	  'table' => 'node',
	  'field' => 'title',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	),
	'field_merci_volunteers_detail_value' => array(
	  'label' => 'Describe Volunteer Needs',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 0,
	  'label_type' => 'widget',
	  'format' => 'default',
	  'multiple' => array(
		'group' => TRUE,
		'multiple_number' => '',
		'multiple_from' => '',
		'multiple_reversed' => FALSE,
	  ),
	  'exclude' => 0,
	  'id' => 'field_merci_volunteers_detail_value',
	  'table' => 'node_data_field_merci_volunteers_detail',
	  'field' => 'field_merci_volunteers_detail_value',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	),
	'edit_node' => array(
	  'label' => 'Edit link',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'text' => '',
	  'exclude' => 0,
	  'id' => 'edit_node',
	  'table' => 'node',
	  'field' => 'edit_node',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	),
  ));
  $handler->override_option('filters', array(
	'type' => array(
	  'operator' => 'in',
	  'value' => array(
		'merci_reservation' => 'merci_reservation',
	  ),
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'type',
	  'table' => 'node',
	  'field' => 'type',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	),
	'status' => array(
	  'operator' => '=',
	  'value' => '0',
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'status',
	  'table' => 'node',
	  'field' => 'status',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	),
	'field_merci_volunteers_value_many_to_one' => array(
	  'operator' => 'or',
	  'value' => array(
		'1' => '1',
	  ),
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'field_merci_volunteers_value_many_to_one',
	  'table' => 'node_data_field_merci_volunteers',
	  'field' => 'field_merci_volunteers_value_many_to_one',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	  'reduce_duplicates' => 0,
	),
  ));
  $handler->override_option('access', array(
	'type' => 'none',
  ));
  $handler->override_option('cache', array(
	'type' => 'none',
  ));
  $handler->override_option('empty', 'No Volunteer Request to Approve');
  $handler->override_option('empty_format', '1');
  $handler->override_option('style_plugin', 'bulk');
  $handler->override_option('style_options', array(
	'grouping' => '',
	'override' => 1,
	'sticky' => 0,
	'order' => 'asc',
	'columns' => array(
	  'edit_node' => 'edit_node',
	  'title' => 'title',
	),
	'info' => array(
	  'edit_node' => array(
		'separator' => '',
	  ),
	  'title' => array(
		'sortable' => 0,
		'separator' => '',
	  ),
	),
	'default' => '-1',
	'execution_type' => '1',
	'display_type' => '0',
	'hide_select_all' => 0,
	'skip_confirmation' => 0,
	'display_result' => 1,
	'merge_single_action' => 1,
	'selected_operations' => array(
	  'node_publish_action' => 'node_publish_action',
	  'flag_nodes:1980f2a3af0b5c3577850d4ba2600405' => 0,
	  'node_assign_owner_action' => 0,
	  'merci_operations_update' => 0,
	  'merci_inventory_operations_update' => 0,
	  'views_bulk_operations_delete_node_action' => 0,
	  'node_mass_update:a27b9efabcd054685a549378b174ad11' => 0,
	  'system_message_action' => 0,
	  'token_actions_message_action' => 0,
	  'views_bulk_operations_ruleset_action_rules_set_1' => 0,
	  'views_bulk_operations_action' => 0,
	  'views_bulk_operations_script_action' => 0,
	  'node_export_node_bulk' => 0,
	  'flag_node_action' => 0,
	  'flag_nodes:805daef3e78653122c2a0e7b3b4d4ade' => 0,
	  'flag_nodes:6fde15a46a35c34bc4deff5701049525' => 0,
	  'flag_nodes:07ba074634c78075cbd0483fc51bb48a' => 0,
	  'flag_nodes:f55cc69af09e839398f09446b29ae072' => 0,
	  'node_make_sticky_action' => 0,
	  'node_make_unsticky_action' => 0,
	  'node_mass_update:c4d3b28efb86fd703619a50b74d43794' => 0,
	  'views_bulk_operations_fields_action' => 0,
	  'views_bulk_operations_taxonomy_action' => 0,
	  'views_bulk_operations_argument_selector_action' => 0,
	  'node_promote_action' => 0,
	  'node_mass_update:14de7d028b4bffdf2b4a266562ca18ac' => 0,
	  'node_mass_update:9c585624b9b3af0b4687d5f97f35e047' => 0,
	  'token_actions_goto_action' => 0,
	  'system_goto_action' => 0,
	  'node_unpromote_action' => 0,
	  'node_mass_update:8ce21b08bb8e773d10018b484fe4815e' => 0,
	  'node_save_action' => 0,
	  'system_send_email_action' => 0,
	  'token_actions_send_email_action' => 0,
	  'creativecommons_set_license_action' => 0,
	  'flag_nodes:76e4c513dfadc798e2eb84ad0a677401' => 0,
	  'flag_nodes:3888e0181b9958b0c111e1f75a5a2d84' => 0,
	  'flag_nodes:e4b5355104be89d65425aa320ed810cd' => 0,
	  'node_mass_update:0ccad85c1ebe4c9ceada1aa64293b080' => 0,
	  'node_unpublish_action' => 0,
	  'node_unpublish_by_keyword_action' => 0,
	  'auto_nodetitle_operations_update' => 0,
	  'pathauto_node_operations_update' => 0,
	),
	'views_bulk_operations_fields_action' => array(
	  'php_code' => 0,
	  'display_fields' => array(
		'_all_' => '_all_',
	  ),
	),
	'vbo_barcode_select' => '-',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/merci/manage/volunteer_requests');
  $handler->override_option('menu', array(
	'type' => 'normal',
	'title' => 'Pending Volunteer Requests',
	'description' => '',
	'weight' => '0',
	'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  $view = new view;
  $view->name = 'merci_signup_volunteer_calendar';
  $view->description = 'A multi-dimensional calendar view with back/next navigation.';
  $view->tag = 'Calendar';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
	'title' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 1,
	  'exclude' => 1,
	  'id' => 'title',
	  'table' => 'node',
	  'field' => 'title',
	  'relationship' => 'none',
	  'override' => array(
		'button' => 'Override',
	  ),
	),
	'field_merci_date_value' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 0,
	  'repeat' => array(
		'show_repeat_rule' => '',
	  ),
	  'multiple' => array(
		'multiple_number' => '',
		'multiple_from' => '',
		'multiple_to' => '',
		'group' => TRUE,
	  ),
	  'fromto' => array(
		'fromto' => 'both',
	  ),
	  'label_type' => 'none',
	  'format' => 'default',
	  'exclude' => 0,
	  'id' => 'field_merci_date_value',
	  'table' => 'node_data_field_merci_date',
	  'field' => 'field_merci_date_value',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	),
	'field_merci_signup_detail_value' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 1,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'link_to_node' => 0,
	  'label_type' => 'none',
	  'format' => 'default',
	  'multiple' => array(
		'group' => TRUE,
		'multiple_number' => '',
		'multiple_from' => '',
		'multiple_reversed' => FALSE,
	  ),
	  'exclude' => 0,
	  'id' => 'field_merci_signup_detail_value',
	  'table' => 'node_data_field_merci_signup_detail',
	  'field' => 'field_merci_signup_detail_value',
	  'relationship' => 'none',
	),
	'node_link' => array(
	  'label' => '',
	  'alter' => array(
		'alter_text' => 0,
		'text' => '',
		'make_link' => 0,
		'path' => '',
		'link_class' => '',
		'alt' => '',
		'prefix' => '',
		'suffix' => '',
		'target' => '',
		'help' => '',
		'trim' => 0,
		'max_length' => '',
		'word_boundary' => 1,
		'ellipsis' => 1,
		'html' => 0,
		'strip_tags' => 0,
	  ),
	  'empty' => '',
	  'hide_empty' => 0,
	  'empty_zero' => 0,
	  'text' => 'Volunteer',
	  'exclude' => 0,
	  'id' => 'node_link',
	  'table' => 'signup',
	  'field' => 'node_link',
	  'override' => array(
		'button' => 'Override',
	  ),
	  'relationship' => 'none',
	  'tab' => 'signup',
	  'destination' => 1,
	),
  ));
  $handler->override_option('arguments', array(
	'date_argument' => array(
	  'default_action' => 'default',
	  'style_plugin' => 'default_summary',
	  'style_options' => array(),
	  'wildcard' => 'all',
	  'wildcard_substitution' => 'All',
	  'title' => '',
	  'breadcrumb' => '',
	  'default_argument_type' => 'date',
	  'default_argument' => '',
	  'validate_type' => 'none',
	  'validate_fail' => 'not found',
	  'date_fields' => array(
		'node_data_field_merci_date.field_merci_date_value' => 'node_data_field_merci_date.field_merci_date_value',
	  ),
	  'year_range' => '-3:+3',
	  'date_method' => 'OR',
	  'granularity' => 'month',
	  'id' => 'date_argument',
	  'table' => 'node',
	  'field' => 'date_argument',
	  'relationship' => 'none',
	  'default_argument_user' => 0,
	  'default_argument_fixed' => '',
	  'default_argument_php' => '',
	  'validate_argument_node_type' => array(
		'webform' => 0,
		'merci_reservation' => 0,
		'merci_inventory' => 0,
		'merci_inventory_master' => 0,
		'om_playback_server' => 0,
		'additional_inventory' => 0,
		'book' => 0,
		'boom_pole' => 0,
		'camera_digital_still' => 0,
		'canon_video_light' => 0,
		'canon_xl1_dv_camera' => 0,
		'class' => 0,
		'class_description' => 0,
		'class_feed' => 0,
		'dv_palmcorder' => 0,
		'editing_computer_equipment' => 0,
		'event' => 0,
		'external_hard_drives' => 0,
		'facilities' => 0,
		'field_monitor' => 0,
		'flip_ultra_hd' => 0,
		'handheld_microphone' => 0,
		'hdv_palmcorder' => 0,
		'headphone' => 0,
		'large_tripod' => 0,
		'lavalier_microphone' => 0,
		'light_kit' => 0,
		'livestream' => 0,
		'medium_tripod' => 0,
		'member_level' => 0,
		'merci_projectors' => 0,
		'minidv_vtr_decks' => 0,
		'monopod' => 0,
		'news_item' => 0,
		'om_airing' => 0,
		'om_crew_request' => 0,
		'om_ext_resource' => 0,
		'om_feed' => 0,
		'om_handbook' => 0,
		'om_lobby_message' => 0,
		'om_project' => 0,
		'om_project_feed' => 0,
		'om_project_type' => 0,
		'om_show' => 0,
		'om_timeslot_event' => 0,
		'om_timeslot_rule' => 0,
		'om_timeslot_server' => 0,
		'om_timeslot_theme' => 0,
		'outdoor_reflector' => 0,
		'page' => 0,
		'palmprephone' => 0,
		'panasonic_dvx100a_dv_camera' => 0,
		'panasonic_gs300' => 0,
		'project_blog' => 0,
		'project_event' => 0,
		'project_wiki' => 0,
		'shotgun_microphone' => 0,
		'small_amp_and_speakers' => 0,
		'small_tripod' => 0,
		'sms_message' => 0,
		'sony_lseries_video_light' => 0,
		'sony_pd170_dvcam' => 0,
		'sony_z1u_hdv_camera' => 0,
		'sony_z5u_hdv_camera' => 0,
		'steadicam' => 0,
		'studios' => 0,
		'studio_accessories_props' => 0,
		'technical_problem' => 0,
		'wide_angle_adapter_sony_z1u' => 0,
		'wireless_lavalier' => 0,
		'xlr_adapter' => 0,
	  ),
	  'validate_argument_node_access' => 0,
	  'validate_argument_nid_type' => 'nid',
	  'validate_argument_vocabulary' => array(
		'39' => 0,
		'7' => 0,
		'23' => 0,
		'14' => 0,
		'6' => 0,
		'9' => 0,
		'27' => 0,
		'20' => 0,
		'3' => 0,
		'25' => 0,
		'28' => 0,
		'5' => 0,
		'38' => 0,
		'24' => 0,
		'21' => 0,
		'34' => 0,
		'8' => 0,
		'37' => 0,
		'10' => 0,
		'35' => 0,
		'17' => 0,
		'18' => 0,
		'16' => 0,
		'22' => 0,
		'4' => 0,
		'26' => 0,
		'41' => 0,
		'19' => 0,
		'40' => 0,
		'31' => 0,
		'32' => 0,
		'30' => 0,
		'36' => 0,
		'33' => 0,
		'12' => 0,
	  ),
	  'validate_argument_type' => 'tid',
	  'validate_argument_php' => '',
	  'validate_user_argument_type' => 'uid',
	  'validate_user_roles' => array(
		'2' => 0,
		'23' => 0,
		'21' => 0,
		'14' => 0,
		'17' => 0,
		'5' => 0,
		'20' => 0,
		'4' => 0,
		'16' => 0,
		'6' => 0,
		'10' => 0,
		'13' => 0,
		'15' => 0,
		'25' => 0,
		'24' => 0,
		'22' => 0,
		'19' => 0,
		'18' => 0,
		'3' => 0,
		'12' => 0,
	  ),
	  'override' => array(
		'button' => 'Override',
	  ),
	  'default_options_div_prefix' => '',
	  'validate_argument_transform' => 0,
	  'validate_user_restrict_roles' => 0,
	  'validate_argument_is_member' => 0,
	  'validate_argument_node_flag_name' => '*relationship*',
	  'validate_argument_node_flag_test' => 'flaggable',
	  'validate_argument_node_flag_id_type' => 'id',
	  'validate_argument_user_flag_name' => '*relationship*',
	  'validate_argument_user_flag_test' => 'flaggable',
	  'validate_argument_user_flag_id_type' => 'id',
	  'validate_argument_signup_status' => 'any',
	  'validate_argument_signup_node_access' => 0,
	),
  ));
  $handler->override_option('filters', array(
	'type' => array(
	  'operator' => 'in',
	  'value' => array(
		'merci_reservation' => 'merci_reservation',
	  ),
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'type',
	  'table' => 'node',
	  'field' => 'type',
	  'relationship' => 'none',
	),
	'signup_disabled' => array(
	  'operator' => '=',
	  'value' => '1',
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'signup_disabled',
	  'table' => 'signup',
	  'field' => 'signup_disabled',
	  'relationship' => 'none',
	),
	'status' => array(
	  'operator' => '=',
	  'value' => '1',
	  'group' => '0',
	  'exposed' => FALSE,
	  'expose' => array(
		'operator' => FALSE,
		'label' => '',
	  ),
	  'id' => 'status',
	  'table' => 'node',
	  'field' => 'status',
	  'relationship' => 'none',
	),
  ));
  $handler->override_option('access', array(
	'type' => 'none',
	'role' => array(),
	'perm' => '',
  ));
  $handler->override_option('cache', array(
	'type' => 'none',
  ));
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 0);
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'calendar_nav');
  $handler->override_option('style_options', array(
	'name_size' => '3',
	'with_weekno' => '0',
  ));
  $handler = $view->new_display('calendar', 'Calendar page', 'calendar_1');
  $handler->override_option('style_options', array());
  $handler->override_option('row_plugin', '');
  $handler->override_option('row_options', NULL);
  $handler->override_option('path', 'merci/volunteer_requests');
  $handler->override_option('menu', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));
  $handler->override_option('calendar_colors', array(
	'page' => '#ffff66',
	'story' => '#668cff',
  ));
  $handler->override_option('calendar_colors_vocabulary', array());
  $handler->override_option('calendar_colors_taxonomy', array());
  $handler->override_option('calendar_colors_group', array());
  $handler->override_option('calendar_popup', 0);
  $handler->override_option('calendar_date_link', '');
  $handler = $view->new_display('calendar_block', 'Calendar block', 'calendar_block_1');
  $handler->override_option('style_options', array());
  $handler->override_option('row_plugin', '');
  $handler->override_option('row_options', NULL);
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('calendar_period', 'Year view', 'calendar_period_1');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
	'display_type' => 'year',
	'name_size' => '1',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('inherit_pager', FALSE);
  $handler->override_option('render_pager', TRUE);
  $handler->override_option('displays', array(
	'calendar_1' => 'calendar_1',
	'default' => 0,
	'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'year');
  $handler = $view->new_display('calendar_period', 'Month view', 'calendar_period_2');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
	'name_size' => '99',
	'with_weekno' => '0',
	'max_items' => '0',
	'max_items_behavior' => 'more',
	'groupby_times' => 'hour',
	'groupby_times_custom' => '',
	'groupby_field' => '',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('inherit_pager', FALSE);
  $handler->override_option('render_pager', TRUE);
  $handler->override_option('displays', array(
	'calendar_1' => 'calendar_1',
	'default' => 0,
	'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'month');
  $handler = $view->new_display('calendar_period', 'Day view', 'calendar_period_3');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
	'display_type' => 'day',
	'name_size' => '99',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('inherit_pager', FALSE);
  $handler->override_option('render_pager', TRUE);
  $handler->override_option('displays', array(
	'calendar_1' => 'calendar_1',
	'default' => 0,
	'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'day');
  $handler = $view->new_display('calendar_period', 'Week view', 'calendar_period_4');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
	'display_type' => 'week',
	'name_size' => '99',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('inherit_pager', FALSE);
  $handler->override_option('render_pager', TRUE);
  $handler->override_option('displays', array(
	'calendar_1' => 'calendar_1',
	'default' => 0,
	'calendar_block_1' => 0,
  ));
  $handler->override_option('calendar_type', 'week');
  $handler = $view->new_display('calendar_period', 'Block view', 'calendar_period_5');
  $handler->override_option('style_plugin', 'calendar_style');
  $handler->override_option('style_options', array(
	'display_type' => 'month',
	'name_size' => '1',
  ));
  $handler->override_option('attachment_position', 'after');
  $handler->override_option('inherit_arguments', TRUE);
  $handler->override_option('inherit_exposed_filters', TRUE);
  $handler->override_option('inherit_pager', FALSE);
  $handler->override_option('render_pager', TRUE);
  $handler->override_option('displays', array(
	'calendar_1' => 0,
	'default' => 0,
	'calendar_block_1' => 'calendar_block_1',
  ));
  $handler->override_option('calendar_type', 'month');
  $handler = $view->new_display('calendar_ical', 'iCal feed', 'calendar_ical_1');
  $handler->override_option('style_plugin', 'ical');
  $handler->override_option('style_options', array(
	'mission_description' => FALSE,
	'description' => '',
	'summary_field' => 'node_title',
	'description_field' => 'node_revisions_body',
	'location_field' => '',
  ));
  $handler->override_option('row_plugin', '');
  $handler->override_option('path', 'calendar/%/ical');
  $handler->override_option('menu', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
	'type' => 'none',
	'title' => '',
	'description' => '',
	'weight' => 0,
	'name' => 'navigation',
  ));
  $handler->override_option('displays', array(
	'calendar_1' => 'calendar_1',
	'default' => 0,
	'calendar_block_1' => 'calendar_block_1',
  ));
  $handler->override_option('sitename_title', FALSE);


  $views[$view->name] = $view;
  
  return $views;
}

